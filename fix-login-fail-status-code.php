<?php
/*
Plugin Name: Fix Login Fail Status Code
Version: 0.1
Description: Overrides WPEngine's fail login status code 403 trowing back a 200 code in the response headers which is the default code returned by WordPress officially. We need this for rightly funtionality of the plugin <strong>SimpleModal Login</strong> and to display in the modal the right error responses to the user if he fails to login by wrong password or username/email.
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/fix-login-fail-status-code
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function flfsc_login_failed_200() {
	status_header( 200 );
}
add_action( 'wp_login_failed', 'flfsc_login_failed_200' );